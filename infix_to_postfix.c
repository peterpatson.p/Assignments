#include<stdio.h>
#include<ctype.h>
#define MAX 50

char pop();
void push(char);
int priority(char);

char stack[MAX];
int top = -1;

char postfix[MAX];
int posttop = -1;
void postpush(char);


void main()
{
    char infix[MAX];
    char *e, x;
    printf("Enter the infix equation : ");
    scanf("%s",infix);
    e = infix;
    printf("\nPostfix equation is : ");
    while(*e != '\0')
    {
        if(isalnum(*e)){
            printf("%c",*e);
            postpush(*e); ///
        }

        else if(*e == '('){
            push(*e);
        }

        else if(*e == ')')
        {
            while((x = pop()) != '('){
                printf("%c", x);
                postpush(x);
            }
        }

        else
        {
            while(priority(stack[top]) >= priority(*e)){
                char temp = pop();
                printf("%c",temp);
                postpush(temp);
            }
            push(*e);
        }
        e++;
    }
    while(top != -1)
    {
        char temp = pop();
        printf("%c",temp);
        postpush(temp);
    }
}

void push(char x)
{
    stack[++top] = x;
}

char pop()
{
    if(top == -1)
        return -1;
    else
        return stack[top--];
}

int priority(char x)
{
    if(x == '(')
        return 0;
    if(x == '+' || x == '-')
        return 1;
    if(x == '*' || x == '/')
        return 2;
}

void postpush(char x)
{
    postfix[++posttop] = x;
}
