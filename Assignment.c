#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#define FILENAME_MAX 10
#define COL 15
#define ROW 50
#define VALUE 50

void file2array(char[FILENAME_MAX]);
void printarray(char [ROW][COL][VALUE],int);
void coursewise(int);


int data[ROW][COL][VALUE];
char* dayNames[8] = {"", "Monday", "Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
char* classNames[5] = {"", "S1", "S2", "S3", "S4"};


void main(){

    char filenameinput[FILENAME_MAX];
    printf("Enter the name of the file\n");
    gets(filenameinput);
    file2array(filenameinput);
    coursewise(4);
}


void file2array(char filename[FILENAME_MAX])
{
    FILE *fp;
    char ch;

    int i=0,j=0,k=0;

    fp = fopen(filename, "r");
    if(fp == NULL){
        printf("\nERROR! File not found.\n");
        exit(0);
    }
    else
    {
        printf("Reading from the file...\n");
        do
        {
            ch = getc(fp);

            if(ch !='\t')
            {
                data[i][j][k] = ch;
                k++;
            }

            if(ch == '\t' || (ch == ' ' &&  j < 4))
            {
                data[i][j][k] = '\0';
                j++;
                k = 0;
            }
            if(ch == '\n'){ i++; j=0;}




        } while(ch != EOF);
    }
    printf("\nData stored in an array!");
    fclose(fp);

}

 void coursewise(int lines)
 {
    char coursenameinput[9];
    char temparr[8];
    int i = 0,j = 5;
    int result = 0;
    int ans;

    printf("\nEnter Course Name : ");

    for(int temp= 0; temp< 9;temp++){
        scanf("%c",&coursenameinput[temp]);
    }
     for(i = 0; i< lines; i++)
    {
        int t = 0;
        while(data[i][j][t] != '\0')
        {
            if(coursenameinput[t] == data[i][j][t]){
                result++;
            }

            else{
                result= 0;
            }
            t++;


        }if(result > 6){
            ans = i;

            }
    }
    ///Final
    int t = 0;

    ///CNo
    printf("\nCourse Number :");
    do{
        printf("%c",data[ans][5][t]);
        t++;
    }while(data[ans][5][t] != '\0');

    ///CName
    printf("\nCourse Name :");
    t = 0;
    do{
        printf("%c",data[ans][6][t]);
        t++;
    }while(data[ans][6][t] != '\0');

    ///II
    printf("\nInstructor Initial :");
    t = 0;
    do{
        printf("%c",data[ans][7][t]);
        t++;
    }while(data[ans][7][t] != '\0');

    ///OF
    printf("\nOffer for :");
    t = 0;
    do{
        printf("%c",data[ans][1][t]);
        t++;
    }while(data[ans][1][t] != '\0');

    ///LO
    printf("\nLecture Hours :");
    t = 0;
    do{
        printf("%c",data[ans][2][t]);
        t++;
    }while(data[ans][2][t] != '\0');
    t = 0;
    do{
        printf("%c",data[ans][3][t]);
        t++;
    }while(data[ans][3][t] != '\0');
    t = 0;
    do{
        printf("%c",data[ans][4][t]);
        t++;
    }while(data[ans][4][t] != '\0');
    printf(" in ");
    t = 0;
    do{
        printf("%c",data[ans][12][t]);
        t++;
    }while(data[ans][12][t] != '\0');
 }
